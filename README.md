# hathor-node-docker

Copy `hathor.env-sample` and paste it to `hathor.env` file and change the variables if necessary.

If you change the variables, make sure to properly change in the `docker-compose.yaml` file.

Change `peer.json` to a new peer id information as well.

Now run the container:
```bash
docker-compose up -d
```

## Generating peer information

```bash
docker run --rm hathornetwork/hathor-core gen_peer_id > peer.json
```
